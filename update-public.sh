auto_push="true"

cp -r images public/
cp -r scripts public/
cp -r styles public/
cp index.html public/

if [ $auto_push == "true" ]
then
    git add * .gitlab-ci.yml
    git commit -S -m "Updated public directory"
    git push -uf jditvoritsazorem main
fi
