let debug = false;

function darkmodetoggle() {
    if(debug) console.log(document.body.style.backgroundColor);
    if(document.body.style.backgroundColor=="rgb(3, 2, 15)") {
        document.body.style.backgroundColor = "white";
        //document.getElementById("text").style.color = "black";
        document.getElementById("tiktokicon").style.textShadow = "2px 2px 4px #000000";
        if(debug) console.log("[LOG] Dark mode OFF");
        document.cookie = "lightmode";
        return 0;
    } else if(document.body.style.backgroundColor=="white") {
        document.body.style.backgroundColor = "#03020f";
        //document.getElementById("text").style.color = "white";
        document.cookie = "darkmode";
        if(debug) console.log("[LOG] Dark mode ON");
        return 0;
    }
}